﻿Public Class frmAdmUsers

    Private Sub frmAdmUsers_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        FPLimpiar()
        lblFechaModificacion.Text = Format(Now, "dd/MM/yyyy HH:mm")
        lblUsuarioModifica.Text = VGUsuario
        ' Obtención de los Niveles de Acceso
        cboNivelAcceso.Items.Clear()
        VGStrSQL = "SELECT * FROM nivel_seguridad ORDER BY ni_nombre"
        Dim adaptador As New OleDb.OleDbDataAdapter(VGStrSQL, cn)
        Dim registro As New DataSet
        adaptador.Fill(registro, "nivel_seguridad")
        Dim NumRegistros As Byte = registro.Tables("nivel_seguridad").Rows.Count
        If NumRegistros = 0 Then
            MessageBox.Show("No se han definido niveles de acceso...", "SIAE", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            ReDim VGCodNivSeg(NumRegistros)
            ' mapeo los registros encontrados
            For i = 0 To NumRegistros - 1
                VGCodNivSeg(i) = registro.Tables("nivel_seguridad").Rows(i).Item("cod_nivseg")
                cboNivelAcceso.Items.Add(registro.Tables("nivel_seguridad").Rows(i).Item("ni_nombre"))
            Next i
            cboNivelAcceso.SelectedIndex = 0
        End If
        Select Case Me.Text
            Case "Nuevo Usuario"
                lblFechaIngreso.Text = Format(Now, "dd/MM/yyyy HH:mm")
                TxtLogin.Focus()
            Case "Modificar Usuario"
                lblCodUsuario.Text = VGCodUsuarioSel
                ' Obtención de los datos del Usuario
                FillFields()
        End Select
    End Sub
    Private Sub FPLimpiar()
        TxtLogin.Text = ""
        TxtNombreCompleto.Text = ""
        TxtPassword.Text = ""
        TxtAlias.Text = ""
        lblFechaIngreso.Text = ""
        lblUsuarioModifica.Text = ""
        lblFechaModificacion.Text = ""
    End Sub
    Private Sub FillFields()
        Dim consulta As String = "SELECT * FROM usuario WHERE cod_usuario = " & Trim(lblCodUsuario.Text)
        Dim adaptador As New OleDb.OleDbDataAdapter(consulta, cn)
        Dim registro As New DataSet
        adaptador.Fill(registro, "usuario")
        Dim NumRegistros As Byte = registro.Tables("usuario").Rows.Count
        If NumRegistros = 0 Then
            MessageBox.Show("Error al consultar el usuario...", "Mensaje de error...", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            TxtLogin.Text = registro.Tables("usuario").Rows(0).Item("us_name")
            TxtNombreCompleto.Text = registro.Tables("usuario").Rows(0).Item("us_fullname")
            TxtPassword.Text = EncryptString(registro.Tables("usuario").Rows(0).Item("us_passwd"), DECRYPT)
            TxtAlias.Text = registro.Tables("usuario").Rows(0).Item("us_alias")
            'lblFechaIngreso.Text = Fecha_a_String(Format(registro.Tables("usuario").Rows(0).Item("us_fecing"), "MM/dd/yyyy"))
            lblFechaIngreso.Text = registro.Tables("usuario").Rows(0).Item("us_fecing")
            lblUsuarioModifica.Text = registro.Tables("usuario").Rows(0).Item("us_modifica")
            lblFechaModificacion.Text = registro.Tables("usuario").Rows(0).Item("us_fecmod")
            optGeneroM.Checked = registro.Tables("usuario").Rows(0).Item("us_sexo") = "M"
            optGeneroF.Checked = registro.Tables("usuario").Rows(0).Item("us_sexo") = "F"
            ' Obtengo el índice en la lista combinada Nivel de Acceso
            For j = 0 To UBound(VGCodNivSeg, 1) - 1
                If Trim(VGCodNivSeg(j)) = Trim(registro.Tables("usuario").Rows(0).Item("cod_nivseg")) Then
                    cboNivelAcceso.SelectedIndex = j
                    Exit For
                End If
            Next j
            TxtLogin.Focus()
        End If
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Select Case Me.Text
            Case "Nuevo Usuario"
                FPIngresar()
            Case "Modificar Usuario"
                FPModificar()
        End Select
    End Sub
    Private Sub FPIngresar()
        ' Procedimiento para ingresar un nuevo usuario
        If Trim(TxtLogin.Text) = "" Then
            MessageBox.Show("Debe ingresar el login del usuario...", "SIAE", MessageBoxButtons.OK, MessageBoxIcon.Information)
            TxtLogin.Focus()
        ElseIf Trim(TxtNombreCompleto.Text) = "" Then
            MessageBox.Show("Debe ingresar el nombre completo del usuario...", "SIAE", MessageBoxButtons.OK, MessageBoxIcon.Information)
            TxtNombreCompleto.Focus()
        ElseIf Trim(TxtPassword.Text) = "" Then
            MessageBox.Show("Debe ingresar la contraseña del usuario...", "SIAE", MessageBoxButtons.OK, MessageBoxIcon.Information)
            TxtPassword.Focus()
        ElseIf Trim(TxtAlias.Text) = "" Then
            MessageBox.Show("Debe ingresar el alias del usuario...", "SIAE", MessageBoxButtons.OK, MessageBoxIcon.Information)
            TxtAlias.Focus()
        ElseIf Not optGeneroM.Checked And Not optGeneroF.Checked Then
            MessageBox.Show("Debe seleccionar el género del usuario...", "SIAE", MessageBoxButtons.OK, MessageBoxIcon.Information)
            optGeneroM.Focus()
        ElseIf FPBuscarCampo("usuario", "us_name", Trim(TxtLogin.Text)) Then
            MsgBox("El login del Usuario ya se encuentra ingresado " & vbCrLf & "en la base de datos, favor verificar.", vbInformation, "Verificar datos de ingreso")
            TxtLogin.Focus()
        Else
            ' Ingresar un nuevo registro en la tabla usuario
            VGStrSQL$ = "INSERT INTO usuario (cod_nivseg, us_name, us_fullname, us_passwd, us_alias, us_sexo, us_fecing, us_modifica, us_fecmod) VALUES("
            ' Nivel de Acceso
            VGStrSQL$ = VGStrSQL$ & Trim(VGCodNivSeg(cboNivelAcceso.SelectedIndex)) & ", '"
            ' Login
            VGStrSQL$ = VGStrSQL$ & Trim(TxtLogin.Text) & "', '"
            ' Nombre Completo
            VGStrSQL$ = VGStrSQL$ & Trim(TxtNombreCompleto.Text) & "', '"
            ' Password Encriptado
            Dim VTPCadPasswd$ = Trim(TxtPassword.Text)
            'Call ENCRYPT(VTPCadPasswd$, VGClave$)
            VTPCadPasswd$ = EncryptString(VTPCadPasswd$, ENCRYPT)
            VGStrSQL$ = VGStrSQL$ & Trim(VTPCadPasswd$) & "', '"
            ' Alias
            VGStrSQL$ = VGStrSQL$ & Trim(TxtAlias.Text) & "', '"
            ' Código de sexo
            Dim VTPCodSexo As String = ""
            If optGeneroM.Checked Then
                VTPCodSexo$ = "M"
            ElseIf optGeneroF.Checked Then
                VTPCodSexo$ = "F"
            End If
            VGStrSQL$ = VGStrSQL$ & VTPCodSexo$ & "', #"
            ' Fecha de Ingreso
            VGStrSQL$ = VGStrSQL$ & Format(Now, "dd/MM/yyyy") & "#, '"
            ' Usuario que modifica el registro
            VGStrSQL$ = VGStrSQL$ & Trim(lblUsuarioModifica.Text) & "', #"
            ' Fecha de Modificación
            VGStrSQL$ = VGStrSQL$ & Format(Now, "dd/MM/yyyy") & "#)"
            Dim comando As New OleDb.OleDbCommand(VGStrSQL, cn)
            comando.ExecuteNonQuery()
            MsgBox("El usuario ha sido insertado exitosamente.", vbInformation, "SIAE")
            'Cierro la ventana luego del mensaje de inserción
            Me.Close()
        End If
    End Sub

    Private Sub FPModificar()
        ' Procedimiento para modificar un usuario
        If Trim(TxtLogin.Text) = "" Then
            MessageBox.Show("Debe ingresar el login del usuario...", "SIAE", MessageBoxButtons.OK, MessageBoxIcon.Information)
            TxtLogin.Focus()
        ElseIf Trim(TxtNombreCompleto.Text) = "" Then
            MessageBox.Show("Debe ingresar el nombre completo del usuario...", "SIAE", MessageBoxButtons.OK, MessageBoxIcon.Information)
            TxtNombreCompleto.Focus()
        ElseIf Trim(TxtPassword.Text) = "" Then
            MessageBox.Show("Debe ingresar la contraseña del usuario...", "SIAE", MessageBoxButtons.OK, MessageBoxIcon.Information)
            TxtPassword.Focus()
        ElseIf Trim(TxtAlias.Text) = "" Then
            MessageBox.Show("Debe ingresar el alias del usuario...", "SIAE", MessageBoxButtons.OK, MessageBoxIcon.Information)
            TxtAlias.Focus()
        ElseIf Not optGeneroM.Checked And Not optGeneroF.Checked Then
            MessageBox.Show("Debe seleccionar el género del usuario...", "SIAE", MessageBoxButtons.OK, MessageBoxIcon.Information)
            optGeneroM.Focus()
        Else
            ' Modificar un registro en la tabla usuario
            VGStrSQL$ = "UPDATE usuario SET"
            ' Nivel de Acceso
            VGStrSQL$ = VGStrSQL$ & " cod_nivseg = " & Trim(VGCodNivSeg(cboNivelAcceso.SelectedIndex)) & ","
            ' Login
            VGStrSQL$ = VGStrSQL$ & " us_name = '" & Trim(TxtLogin.Text) & "',"
            ' Nombre Completo
            VGStrSQL$ = VGStrSQL$ & " us_fullname = '" & Trim(TxtNombreCompleto.Text) & "',"
            ' Password Encriptado
            Dim VTPCadPasswd$ = Trim(TxtPassword.Text)
            'Call ENCRYPT(VTPCadPasswd$, VGClave$)
            VTPCadPasswd$ = EncryptString(VTPCadPasswd$, ENCRYPT)
            VGStrSQL$ = VGStrSQL$ & " us_passwd = '" & Trim(VTPCadPasswd$) & "',"
            ' Alias
            VGStrSQL$ = VGStrSQL$ & " us_alias = '" & Trim(TxtAlias.Text) & "',"
            ' Código de sexo
            Dim VTPCodSexo As String = ""
            If optGeneroM.Checked Then
                VTPCodSexo$ = "M"
            ElseIf optGeneroF.Checked Then
                VTPCodSexo$ = "F"
            End If
            VGStrSQL$ = VGStrSQL$ & " us_sexo = '" & VTPCodSexo$ & "',"
            ' Usuario que modifica el registro
            VGStrSQL$ = VGStrSQL$ & " us_modifica = '" & Trim(lblUsuarioModifica.Text) & "',"
            ' Fecha de Modificación
            VGStrSQL$ = VGStrSQL$ & " us_fecmod = #" & Format(Now, "dd/MM/yyyy") & "#"
            ' Cláusula WHERE
            VGStrSQL$ = VGStrSQL$ & " WHERE cod_usuario = " & Trim(lblCodUsuario.Text)
            Dim comando As New OleDb.OleDbCommand(VGStrSQL, cn)
            comando.ExecuteNonQuery()
            MsgBox("El usuario ha sido modificado exitosamente.", vbInformation, "SIAE")
            'Cierro la ventana luego del mensaje de inserción
            Me.Close()
        End If
    End Sub
End Class