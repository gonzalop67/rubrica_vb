﻿Public Class frmSplash

    Dim Contador As Long = 0 ' Variable utilizada para el efecto del progressbar

    Private Sub frmSplash_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblVersion.Text = "Versión: " & My.Application.Info.Version.Major & "." & My.Application.Info.Version.Minor & "." & My.Application.Info.Version.Revision 'Application.ProductVersion
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        If ProgressBar1.Value < 100 Then
            Contador = Contador + 20
            ProgressBar1.Value = Contador
        Else
            Me.Hide()
            frmLogin.Show()
            Timer1.Enabled = False
        End If
    End Sub
End Class
