﻿Public Class frmAdmAreas
    Private Sub FrmAdmAreas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        FPLimpiar()
        Select Case Me.Text
            Case "Nueva Area"
                TxtNombre.Focus()
            Case "Modificar Area"
                lblCodArea.Text = VGCodAreaSel
                ' Obtención de los datos del Area
                FillFields()
        End Select
    End Sub

    Private Sub FPLimpiar()
        lblCodArea.Text = ""
        TxtNombre.Text = ""
    End Sub

    Private Sub FillFields()
        Dim consulta As String = "SELECT * FROM area WHERE id_area = " & Trim(lblCodArea.Text)
        Dim adaptador As New OleDb.OleDbDataAdapter(consulta, cn)
        Dim registro As New DataSet
        adaptador.Fill(registro, "area")
        Dim NumRegistros As Byte = registro.Tables("area").Rows.Count
        If NumRegistros = 0 Then
            MessageBox.Show("Error al consultar el area...", "Mensaje de error...", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            TxtNombre.Text = registro.Tables("area").Rows(0).Item("ar_nombre")
            TxtNombre.Focus()
        End If
    End Sub

    Private Sub BtnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Select Case Me.Text
            Case "Nueva Area"
                FPIngresar()
            Case "Modificar Area"
                FPModificar()
        End Select
    End Sub

    Private Sub FPIngresar()
        ' Procedimiento para ingresar una nueva area
        If Trim(TxtNombre.Text) = "" Then
            MessageBox.Show("Debe ingresar el nombre del área...", "SIAE", MessageBoxButtons.OK, MessageBoxIcon.Information)
            TxtNombre.Focus()
        ElseIf FPBuscarCampo("area", "ar_nombre", Trim(TxtNombre.Text)) Then
            MsgBox("El nombre del Area ya se encuentra ingresado " & vbCrLf & "en la base de datos, favor verificar.", vbInformation, "Verificar datos de ingreso")
            TxtNombre.Focus()
        Else
            ' Ingresar un nuevo registro en la tabla area
            VGStrSQL$ = "INSERT INTO area (ar_nombre) VALUES("
            ' Nombre
            VGStrSQL$ = VGStrSQL$ & "'" & Trim(TxtNombre.Text) & "')"
            Dim comando As New OleDb.OleDbCommand(VGStrSQL, cn)
            comando.ExecuteNonQuery()
            MsgBox("El área ha sido insertada exitosamente.", vbInformation, "SIAE")
            'Cierro la ventana luego del mensaje de inserción
            Me.Close()
        End If
    End Sub

    Private Sub FPModificar()
        ' Procedimiento para modificar un usuario
        If Trim(TxtNombre.Text) = "" Then
            MessageBox.Show("Debe ingresar el nombre del area...", "SIAE", MessageBoxButtons.OK, MessageBoxIcon.Information)
            TxtNombre.Focus()
        Else
            ' Modificar un registro en la tabla area
            VGStrSQL$ = "UPDATE area SET"
            ' Nombre
            VGStrSQL$ = VGStrSQL$ & " ar_nombre = '" & Trim(TxtNombre.Text) & "'"
            ' Cláusula WHERE
            VGStrSQL$ = VGStrSQL$ & " WHERE id_area = " & Trim(lblCodArea.Text)
            Dim comando As New OleDb.OleDbCommand(VGStrSQL, cn)
            comando.ExecuteNonQuery()
            MsgBox("El área ha sido modificada exitosamente.", vbInformation, "SIAE")
            'Cierro la ventana luego del mensaje de inserción
            Me.Close()
        End If
    End Sub
End Class