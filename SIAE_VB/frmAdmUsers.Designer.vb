﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAdmUsers
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAdmUsers))
        Me.lblCodUsuario = New System.Windows.Forms.Label()
        Me.lblFechaIngreso = New System.Windows.Forms.Label()
        Me.lblFechaModificacion = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.optGeneroF = New System.Windows.Forms.RadioButton()
        Me.optGeneroM = New System.Windows.Forms.RadioButton()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.lblUsuarioModifica = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cboNivelAcceso = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TxtAlias = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TxtPassword = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TxtNombreCompleto = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.TxtLogin = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lblCodUsuario
        '
        Me.lblCodUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCodUsuario.Location = New System.Drawing.Point(129, 5)
        Me.lblCodUsuario.Name = "lblCodUsuario"
        Me.lblCodUsuario.Size = New System.Drawing.Size(100, 20)
        Me.lblCodUsuario.TabIndex = 95
        Me.lblCodUsuario.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFechaIngreso
        '
        Me.lblFechaIngreso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFechaIngreso.Location = New System.Drawing.Point(129, 148)
        Me.lblFechaIngreso.Name = "lblFechaIngreso"
        Me.lblFechaIngreso.Size = New System.Drawing.Size(376, 20)
        Me.lblFechaIngreso.TabIndex = 94
        Me.lblFechaIngreso.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFechaModificacion
        '
        Me.lblFechaModificacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblFechaModificacion.Location = New System.Drawing.Point(129, 197)
        Me.lblFechaModificacion.Name = "lblFechaModificacion"
        Me.lblFechaModificacion.Size = New System.Drawing.Size(254, 20)
        Me.lblFechaModificacion.TabIndex = 93
        Me.lblFechaModificacion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(3, 200)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(120, 13)
        Me.Label12.TabIndex = 92
        Me.Label12.Text = "Fecha de Modificación:"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'optGeneroF
        '
        Me.optGeneroF.AutoSize = True
        Me.optGeneroF.Location = New System.Drawing.Point(437, 198)
        Me.optGeneroF.Name = "optGeneroF"
        Me.optGeneroF.Size = New System.Drawing.Size(71, 17)
        Me.optGeneroF.TabIndex = 81
        Me.optGeneroF.TabStop = True
        Me.optGeneroF.Text = "Femenino"
        Me.optGeneroF.UseVisualStyleBackColor = True
        '
        'optGeneroM
        '
        Me.optGeneroM.AutoSize = True
        Me.optGeneroM.Location = New System.Drawing.Point(437, 175)
        Me.optGeneroM.Name = "optGeneroM"
        Me.optGeneroM.Size = New System.Drawing.Size(73, 17)
        Me.optGeneroM.TabIndex = 80
        Me.optGeneroM.TabStop = True
        Me.optGeneroM.Text = "Masculino"
        Me.optGeneroM.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(389, 177)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(45, 13)
        Me.Label10.TabIndex = 91
        Me.Label10.Text = "Género:"
        '
        'lblUsuarioModifica
        '
        Me.lblUsuarioModifica.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblUsuarioModifica.Location = New System.Drawing.Point(129, 173)
        Me.lblUsuarioModifica.Name = "lblUsuarioModifica"
        Me.lblUsuarioModifica.Size = New System.Drawing.Size(254, 20)
        Me.lblUsuarioModifica.TabIndex = 90
        Me.lblUsuarioModifica.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(3, 176)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(120, 13)
        Me.Label8.TabIndex = 89
        Me.Label8.Text = "Modificado por:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(3, 152)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(120, 13)
        Me.Label7.TabIndex = 88
        Me.Label7.Text = "Fecha de Ingreso:"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cboNivelAcceso
        '
        Me.cboNivelAcceso.FormattingEnabled = True
        Me.cboNivelAcceso.Location = New System.Drawing.Point(129, 123)
        Me.cboNivelAcceso.Name = "cboNivelAcceso"
        Me.cboNivelAcceso.Size = New System.Drawing.Size(376, 21)
        Me.cboNivelAcceso.TabIndex = 79
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(3, 126)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(120, 13)
        Me.Label6.TabIndex = 87
        Me.Label6.Text = "Nivel de Acceso:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'TxtAlias
        '
        Me.TxtAlias.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtAlias.Location = New System.Drawing.Point(129, 99)
        Me.TxtAlias.Name = "TxtAlias"
        Me.TxtAlias.Size = New System.Drawing.Size(376, 20)
        Me.TxtAlias.TabIndex = 78
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(3, 102)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(120, 13)
        Me.Label5.TabIndex = 86
        Me.Label5.Text = "Alias:"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'TxtPassword
        '
        Me.TxtPassword.Location = New System.Drawing.Point(129, 75)
        Me.TxtPassword.Name = "TxtPassword"
        Me.TxtPassword.Size = New System.Drawing.Size(376, 20)
        Me.TxtPassword.TabIndex = 77
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(3, 78)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(120, 13)
        Me.Label4.TabIndex = 85
        Me.Label4.Text = "Contraseña:"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'TxtNombreCompleto
        '
        Me.TxtNombreCompleto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtNombreCompleto.Location = New System.Drawing.Point(129, 51)
        Me.TxtNombreCompleto.Name = "TxtNombreCompleto"
        Me.TxtNombreCompleto.Size = New System.Drawing.Size(376, 20)
        Me.TxtNombreCompleto.TabIndex = 75
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(3, 54)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(120, 13)
        Me.Label3.TabIndex = 82
        Me.Label3.Text = "Nombre Completo:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'btnCancelar
        '
        Me.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancelar.Location = New System.Drawing.Point(508, 27)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 84
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(508, 3)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 83
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'TxtLogin
        '
        Me.TxtLogin.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TxtLogin.Location = New System.Drawing.Point(129, 27)
        Me.TxtLogin.Name = "TxtLogin"
        Me.TxtLogin.Size = New System.Drawing.Size(376, 20)
        Me.TxtLogin.TabIndex = 74
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(3, 31)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(120, 13)
        Me.Label2.TabIndex = 76
        Me.Label2.Text = "Login:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(3, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(120, 23)
        Me.Label1.TabIndex = 73
        Me.Label1.Text = "Código Usuario:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'frmAdmUsers
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(586, 220)
        Me.Controls.Add(Me.lblCodUsuario)
        Me.Controls.Add(Me.lblFechaIngreso)
        Me.Controls.Add(Me.lblFechaModificacion)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.optGeneroF)
        Me.Controls.Add(Me.optGeneroM)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.lblUsuarioModifica)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.cboNivelAcceso)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.TxtAlias)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.TxtPassword)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.TxtNombreCompleto)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.TxtLogin)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAdmUsers"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ingreso de Usuarios"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblCodUsuario As System.Windows.Forms.Label
    Friend WithEvents lblFechaIngreso As System.Windows.Forms.Label
    Friend WithEvents lblFechaModificacion As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents optGeneroF As System.Windows.Forms.RadioButton
    Friend WithEvents optGeneroM As System.Windows.Forms.RadioButton
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioModifica As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cboNivelAcceso As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TxtAlias As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TxtPassword As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TxtNombreCompleto As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents TxtLogin As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
