﻿Public Class frmPersonalizar

    Dim VLIdJornada() As Integer

    Private Sub frmPersonalizar_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ToolTip1.SetToolTip(PictureBox1, "Haga doble clic para cambiar la imagen")
        ' Aquí cargo las jornadas definidas en la base de datos
        Dim query As String = "SELECT * FROM jornada"
        Dim adapter As New OleDb.OleDbDataAdapter(query, cn)
        Dim record As New DataSet
        adapter.Fill(record, "jornadas")
        Dim NumRegistros As Byte = record.Tables("jornadas").Rows.Count
        ReDim VLIdJornada(NumRegistros)
        CboJornada.Items.Clear()
        If NumRegistros <> 0 Then
            ' mapeo los registros encontrados
            For i = 0 To NumRegistros - 1
                CboJornada.Items.Add(record.Tables("jornadas").Rows(i).Item("jo_nombre"))
                VLIdJornada(i) = record.Tables("jornadas").Rows(i).Item("id_jornada")
            Next i
        End If
        ' Aquí cargo los datos de la institución educativa
        Dim consulta As String = "SELECT * FROM institucion"
        Dim adaptador As New OleDb.OleDbDataAdapter(consulta, cn)
        Dim registro As New DataSet
        adaptador.Fill(registro, "institucion")
        Dim lista As Byte = registro.Tables("institucion").Rows.Count
        If lista = 0 Then
            MessageBox.Show("Institución no definida...")
        Else
            TxtNombreInstitucion.Text = registro.Tables("institucion").Rows(0).Item("nombre")
            TxtCodigoAMIE.Text = registro.Tables("institucion").Rows(0).Item("codigo_amie")
            TxtPaginaWeb.Text = registro.Tables("institucion").Rows(0).Item("pagina_web")
            TxtTelefonos.Text = registro.Tables("institucion").Rows(0).Item("telefonos")
            TxtNombreAutoridad.Text = registro.Tables("institucion").Rows(0).Item("director_rector")
            TxtCicloEscolar.Text = registro.Tables("institucion").Rows(0).Item("ciclo_escolar")
            Dim IdJornada As Integer = registro.Tables("institucion").Rows(0).Item("id_jornada")
            'Procedimiento para recuperar el índice que corresponde al tipo de activo
            For j = 0 To UBound(VLIdJornada, 1) - 1
                If Trim(VLIdJornada(j)) = IdJornada Then
                    CboJornada.SelectedIndex = j
                    Exit For
                End If
            Next j
        End If
    End Sub

    Private Sub btnRegistrarUsuarios_Click(sender As Object, e As EventArgs) Handles btnRegistrarUsuarios.Click
        ' Llamada a la ventana modal para registrar usuarios
        frmUsuarios.ShowDialog()
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnRegistrarAreas_Click(sender As Object, e As EventArgs) Handles btnRegistrarAreas.Click
        ' Llamada a la ventana modal para registrar areas
        frmAreas.ShowDialog()
    End Sub

    Private Sub btnActualizar_Click(sender As Object, e As EventArgs) Handles btnActualizar.Click
        ' Procedimiento para actualizar los datos de la institución educativa
        If Trim(TxtNombreInstitucion.Text) = "" Then
            MessageBox.Show("Debe ingresar el nombre de la institución educativa...", "SIAE", MessageBoxButtons.OK, MessageBoxIcon.Information)
            TxtNombreInstitucion.Focus()
        ElseIf Trim(TxtCodigoAMIE.Text) = "" Then
            MessageBox.Show("Debe ingresar el código AMIE de la institución educativa...", "SIAE", MessageBoxButtons.OK, MessageBoxIcon.Information)
            TxtCodigoAMIE.Focus()
        ElseIf Trim(TxtTelefonos.Text) = "" Then
            MessageBox.Show("Debe ingresar los números telefónicos de la institución educativa...", "SIAE", MessageBoxButtons.OK, MessageBoxIcon.Information)
            TxtTelefonos.Focus()
        ElseIf Trim(TxtNombreAutoridad.Text) = "" Then
            MessageBox.Show("Debe ingresar el nombre de la máxima autoridad de la institución educativa...", "SIAE", MessageBoxButtons.OK, MessageBoxIcon.Information)
            TxtNombreAutoridad.Focus()
        Else
            ' Actualizar los datos informativos de la institución educativa
            VGStrSQL$ = "UPDATE institucion SET"
            ' Nombre de la Institución Educativa
            VGStrSQL$ = VGStrSQL$ & " nombre = '" & Trim(TxtNombreInstitucion.Text) & "',"
            ' Código AMIE de la Institución Educativa
            VGStrSQL$ = VGStrSQL$ & " codigo_amie = '" & Trim(TxtCodigoAMIE.Text) & "',"
            ' Teléfonos de la Institución Educativa
            VGStrSQL$ = VGStrSQL$ & " telefonos = '" & Trim(TxtTelefonos.Text) & "',"
            ' Página web de la Institución Educativa
            VGStrSQL$ = VGStrSQL$ & " pagina_web = '" & Trim(TxtPaginaWeb.Text) & "',"
            ' Nombre de la autoridad máxima de la Institución Educativa
            VGStrSQL$ = VGStrSQL$ & " director_rector = '" & Trim(TxtNombreAutoridad.Text) & "'"
            ' Cláusula WHERE
            VGStrSQL$ = VGStrSQL$ & " WHERE id_institucion = 1"
            Dim comando As New OleDb.OleDbCommand(VGStrSQL, cn)
            comando.ExecuteNonQuery()
            MsgBox("Los datos de la institución educativa han sido actualizados exitosamente.", vbInformation, "SIAE")
            'Cierro la ventana luego del mensaje de inserción
            Me.Close()
        End If
    End Sub
End Class