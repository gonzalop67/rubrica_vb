﻿Public Class frmAreas

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub FrmAreas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Consulta de todas las áreas definidas
        FPQueryAll()
    End Sub

    Private Sub FPQueryAll()
        ' Procedimiento para mostrar todas las áreas definidos
        Dim sql As String
        sql = "SELECT id_area AS Código, ar_nombre AS Nombre" &
               " FROM area ORDER BY ar_nombre"
        Dim da As New OleDb.OleDbDataAdapter(sql, cn)
        Dim ds As New DataSet
        da.Fill(ds)
        dtgAreas.DataSource = ds.Tables(0).DefaultView
        'Ancho de las columnas
        dtgAreas.Columns(0).Width = 50    'Código
        dtgAreas.Columns(1).Width = 280   'Nombre
    End Sub

    Private Sub BtnAñadir_Click(sender As Object, e As EventArgs) Handles btnAñadir.Click
        frmAdmAreas.Text = "Nueva Area"
        frmAdmAreas.ShowDialog()
        FPQueryAll()
    End Sub

    Private Sub BtnActualizar_Click(sender As Object, e As EventArgs) Handles btnActualizar.Click
        VGCodAreaSel = dtgAreas.CurrentRow.Cells(0).Value
        frmAdmAreas.Text = "Modificar Area"
        frmAdmAreas.ShowDialog()
        FPQueryAll()
    End Sub

    Private Sub DtgAreas_DoubleClick(sender As Object, e As EventArgs) Handles dtgAreas.DoubleClick
        VGCodAreaSel = dtgAreas.CurrentRow.Cells(0).Value
        frmAdmAreas.Text = "Modificar Area"
        frmAdmAreas.ShowDialog()
        FPQueryAll()
    End Sub

    Private Sub BtnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        ' Procedimiento para eliminar un registro en la tabla area
        If MsgBox("¿Está seguro que desea eliminar el registro actual?", vbOKCancel + vbQuestion, "Confirmación") = vbOK Then
            Try
                VGCodAreaSel = dtgAreas.CurrentRow.Cells(0).Value
                VGStrSQL$ = "DELETE * FROM area WHERE id_area = " & VGCodAreaSel
                ' Ejecuta la sentencia SQL
                Dim comando As New OleDb.OleDbCommand(VGStrSQL, cn)
                comando.ExecuteNonQuery()
                MsgBox("El área ha sido eliminada exitosamente", vbInformation, "SIAE")
                FPQueryAll()
            Catch ex As Exception
                MsgBox("ERROR: " & ex.Message, vbExclamation, "SIAE")
            End Try
        End If
    End Sub
End Class