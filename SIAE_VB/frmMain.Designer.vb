﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.lblCodAmie = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.btnParalelos = New System.Windows.Forms.Button()
        Me.btnCalificaciones = New System.Windows.Forms.Button()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.btnExpedientes = New System.Windows.Forms.Button()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.btnAsignaturas = New System.Windows.Forms.Button()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.btnDECE = New System.Windows.Forms.Button()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.btnMatricular = New System.Windows.Forms.Button()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.btnReportes = New System.Windows.Forms.Button()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.btnEstadistica = New System.Windows.Forms.Button()
        Me.PictureBox9 = New System.Windows.Forms.PictureBox()
        Me.btnPersonalizar = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(5, 4)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(92, 89)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'lblNombre
        '
        Me.lblNombre.Font = New System.Drawing.Font("Arial Black", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre.Location = New System.Drawing.Point(2, 17)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(621, 35)
        Me.lblNombre.TabIndex = 1
        Me.lblNombre.Text = "UNIDAD EDUCATIVA PCEI FISCAL SALAMANCA"
        Me.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCodAmie
        '
        Me.lblCodAmie.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCodAmie.Location = New System.Drawing.Point(501, 57)
        Me.lblCodAmie.Name = "lblCodAmie"
        Me.lblCodAmie.Size = New System.Drawing.Size(105, 23)
        Me.lblCodAmie.TabIndex = 2
        Me.lblCodAmie.Text = "17H00215"
        Me.lblCodAmie.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'PictureBox2
        '
        Me.PictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(5, 113)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(48, 48)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 3
        Me.PictureBox2.TabStop = False
        '
        'btnParalelos
        '
        Me.btnParalelos.BackColor = System.Drawing.Color.DarkSeaGreen
        Me.btnParalelos.FlatAppearance.BorderSize = 0
        Me.btnParalelos.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnParalelos.Font = New System.Drawing.Font("Arial Black", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnParalelos.ForeColor = System.Drawing.Color.White
        Me.btnParalelos.Location = New System.Drawing.Point(59, 113)
        Me.btnParalelos.Name = "btnParalelos"
        Me.btnParalelos.Size = New System.Drawing.Size(220, 48)
        Me.btnParalelos.TabIndex = 4
        Me.btnParalelos.Text = "PARALELOS"
        Me.btnParalelos.UseVisualStyleBackColor = False
        '
        'btnCalificaciones
        '
        Me.btnCalificaciones.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.btnCalificaciones.FlatAppearance.BorderSize = 0
        Me.btnCalificaciones.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnCalificaciones.Font = New System.Drawing.Font("Arial Black", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCalificaciones.ForeColor = System.Drawing.Color.White
        Me.btnCalificaciones.Location = New System.Drawing.Point(379, 113)
        Me.btnCalificaciones.Name = "btnCalificaciones"
        Me.btnCalificaciones.Size = New System.Drawing.Size(220, 48)
        Me.btnCalificaciones.TabIndex = 6
        Me.btnCalificaciones.Text = "CALIFICACIONES"
        Me.btnCalificaciones.UseVisualStyleBackColor = False
        '
        'PictureBox3
        '
        Me.PictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(325, 113)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(48, 48)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 5
        Me.PictureBox3.TabStop = False
        '
        'btnExpedientes
        '
        Me.btnExpedientes.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnExpedientes.FlatAppearance.BorderSize = 0
        Me.btnExpedientes.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnExpedientes.Font = New System.Drawing.Font("Arial Black", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExpedientes.ForeColor = System.Drawing.Color.White
        Me.btnExpedientes.Location = New System.Drawing.Point(379, 167)
        Me.btnExpedientes.Name = "btnExpedientes"
        Me.btnExpedientes.Size = New System.Drawing.Size(220, 48)
        Me.btnExpedientes.TabIndex = 10
        Me.btnExpedientes.Text = "EXPEDIENTES"
        Me.btnExpedientes.UseVisualStyleBackColor = False
        '
        'PictureBox4
        '
        Me.PictureBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(325, 167)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(48, 48)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox4.TabIndex = 9
        Me.PictureBox4.TabStop = False
        '
        'btnAsignaturas
        '
        Me.btnAsignaturas.BackColor = System.Drawing.Color.MediumAquamarine
        Me.btnAsignaturas.FlatAppearance.BorderSize = 0
        Me.btnAsignaturas.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnAsignaturas.Font = New System.Drawing.Font("Arial Black", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAsignaturas.ForeColor = System.Drawing.Color.White
        Me.btnAsignaturas.Location = New System.Drawing.Point(59, 167)
        Me.btnAsignaturas.Name = "btnAsignaturas"
        Me.btnAsignaturas.Size = New System.Drawing.Size(220, 48)
        Me.btnAsignaturas.TabIndex = 8
        Me.btnAsignaturas.Text = "ASIGNATURAS"
        Me.btnAsignaturas.UseVisualStyleBackColor = False
        '
        'PictureBox5
        '
        Me.PictureBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(5, 167)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(48, 48)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox5.TabIndex = 7
        Me.PictureBox5.TabStop = False
        '
        'btnDECE
        '
        Me.btnDECE.BackColor = System.Drawing.Color.SteelBlue
        Me.btnDECE.FlatAppearance.BorderSize = 0
        Me.btnDECE.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnDECE.Font = New System.Drawing.Font("Arial Black", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDECE.ForeColor = System.Drawing.Color.White
        Me.btnDECE.Location = New System.Drawing.Point(379, 221)
        Me.btnDECE.Name = "btnDECE"
        Me.btnDECE.Size = New System.Drawing.Size(220, 48)
        Me.btnDECE.TabIndex = 14
        Me.btnDECE.Text = "D.E.C.E."
        Me.btnDECE.UseVisualStyleBackColor = False
        '
        'PictureBox6
        '
        Me.PictureBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox6.Image = CType(resources.GetObject("PictureBox6.Image"), System.Drawing.Image)
        Me.PictureBox6.Location = New System.Drawing.Point(325, 221)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(48, 48)
        Me.PictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox6.TabIndex = 13
        Me.PictureBox6.TabStop = False
        '
        'btnMatricular
        '
        Me.btnMatricular.BackColor = System.Drawing.Color.DarkCyan
        Me.btnMatricular.FlatAppearance.BorderSize = 0
        Me.btnMatricular.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnMatricular.Font = New System.Drawing.Font("Arial Black", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMatricular.ForeColor = System.Drawing.Color.White
        Me.btnMatricular.Location = New System.Drawing.Point(59, 221)
        Me.btnMatricular.Name = "btnMatricular"
        Me.btnMatricular.Size = New System.Drawing.Size(220, 48)
        Me.btnMatricular.TabIndex = 12
        Me.btnMatricular.Text = "MATRICULAR"
        Me.btnMatricular.UseVisualStyleBackColor = False
        '
        'PictureBox7
        '
        Me.PictureBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox7.Image = CType(resources.GetObject("PictureBox7.Image"), System.Drawing.Image)
        Me.PictureBox7.Location = New System.Drawing.Point(5, 221)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(48, 48)
        Me.PictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox7.TabIndex = 11
        Me.PictureBox7.TabStop = False
        '
        'btnReportes
        '
        Me.btnReportes.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnReportes.FlatAppearance.BorderSize = 0
        Me.btnReportes.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnReportes.Font = New System.Drawing.Font("Arial Black", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReportes.ForeColor = System.Drawing.Color.White
        Me.btnReportes.Location = New System.Drawing.Point(379, 275)
        Me.btnReportes.Name = "btnReportes"
        Me.btnReportes.Size = New System.Drawing.Size(220, 48)
        Me.btnReportes.TabIndex = 18
        Me.btnReportes.Text = "REPORTES"
        Me.btnReportes.UseVisualStyleBackColor = False
        '
        'PictureBox8
        '
        Me.PictureBox8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox8.Image = CType(resources.GetObject("PictureBox8.Image"), System.Drawing.Image)
        Me.PictureBox8.Location = New System.Drawing.Point(325, 275)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(48, 48)
        Me.PictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox8.TabIndex = 17
        Me.PictureBox8.TabStop = False
        '
        'btnEstadistica
        '
        Me.btnEstadistica.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnEstadistica.FlatAppearance.BorderSize = 0
        Me.btnEstadistica.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnEstadistica.Font = New System.Drawing.Font("Arial Black", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEstadistica.ForeColor = System.Drawing.Color.White
        Me.btnEstadistica.Location = New System.Drawing.Point(59, 275)
        Me.btnEstadistica.Name = "btnEstadistica"
        Me.btnEstadistica.Size = New System.Drawing.Size(220, 48)
        Me.btnEstadistica.TabIndex = 16
        Me.btnEstadistica.Text = "ESTADISTICA"
        Me.btnEstadistica.UseVisualStyleBackColor = False
        '
        'PictureBox9
        '
        Me.PictureBox9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox9.Image = CType(resources.GetObject("PictureBox9.Image"), System.Drawing.Image)
        Me.PictureBox9.Location = New System.Drawing.Point(5, 275)
        Me.PictureBox9.Name = "PictureBox9"
        Me.PictureBox9.Size = New System.Drawing.Size(48, 48)
        Me.PictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox9.TabIndex = 15
        Me.PictureBox9.TabStop = False
        '
        'btnPersonalizar
        '
        Me.btnPersonalizar.BackColor = System.Drawing.Color.DodgerBlue
        Me.btnPersonalizar.FlatAppearance.BorderSize = 0
        Me.btnPersonalizar.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnPersonalizar.Font = New System.Drawing.Font("Arial Black", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPersonalizar.ForeColor = System.Drawing.Color.White
        Me.btnPersonalizar.Location = New System.Drawing.Point(5, 346)
        Me.btnPersonalizar.Name = "btnPersonalizar"
        Me.btnPersonalizar.Size = New System.Drawing.Size(540, 48)
        Me.btnPersonalizar.TabIndex = 19
        Me.btnPersonalizar.Text = "PERSONALIZAR APLICACIÓN"
        Me.btnPersonalizar.UseVisualStyleBackColor = False
        '
        'btnSalir
        '
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnSalir.Image = CType(resources.GetObject("btnSalir.Image"), System.Drawing.Image)
        Me.btnSalir.Location = New System.Drawing.Point(551, 346)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(48, 48)
        Me.btnSalir.TabIndex = 20
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CancelButton = Me.btnSalir
        Me.ClientSize = New System.Drawing.Size(623, 404)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnPersonalizar)
        Me.Controls.Add(Me.btnReportes)
        Me.Controls.Add(Me.PictureBox8)
        Me.Controls.Add(Me.btnEstadistica)
        Me.Controls.Add(Me.PictureBox9)
        Me.Controls.Add(Me.btnDECE)
        Me.Controls.Add(Me.PictureBox6)
        Me.Controls.Add(Me.btnMatricular)
        Me.Controls.Add(Me.PictureBox7)
        Me.Controls.Add(Me.btnExpedientes)
        Me.Controls.Add(Me.PictureBox4)
        Me.Controls.Add(Me.btnAsignaturas)
        Me.Controls.Add(Me.PictureBox5)
        Me.Controls.Add(Me.btnCalificaciones)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.btnParalelos)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.lblCodAmie)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.lblNombre)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Nombre del Software"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblNombre As System.Windows.Forms.Label
    Friend WithEvents lblCodAmie As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents btnParalelos As System.Windows.Forms.Button
    Friend WithEvents btnCalificaciones As System.Windows.Forms.Button
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents btnExpedientes As System.Windows.Forms.Button
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents btnAsignaturas As System.Windows.Forms.Button
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents btnDECE As System.Windows.Forms.Button
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents btnMatricular As System.Windows.Forms.Button
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents btnReportes As System.Windows.Forms.Button
    Friend WithEvents PictureBox8 As System.Windows.Forms.PictureBox
    Friend WithEvents btnEstadistica As System.Windows.Forms.Button
    Friend WithEvents PictureBox9 As System.Windows.Forms.PictureBox
    Friend WithEvents btnPersonalizar As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
End Class
