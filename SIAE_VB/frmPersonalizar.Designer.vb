﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPersonalizar
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPersonalizar))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TxtNombreInstitucion = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TxtCodigoAMIE = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.CboJornada = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TxtPaginaWeb = New System.Windows.Forms.TextBox()
        Me.TxtTelefonos = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TxtCicloEscolar = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TxtNombreAutoridad = New System.Windows.Forms.TextBox()
        Me.btnRegistrarUsuarios = New System.Windows.Forms.Button()
        Me.btnActualizar = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnRegistrarAreas = New System.Windows.Forms.Button()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(9, -1)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(507, 23)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "NOMBRE DE LA INSTITUCIÓN"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TxtNombreInstitucion
        '
        Me.TxtNombreInstitucion.Location = New System.Drawing.Point(12, 25)
        Me.TxtNombreInstitucion.Name = "TxtNombreInstitucion"
        Me.TxtNombreInstitucion.Size = New System.Drawing.Size(504, 20)
        Me.TxtNombreInstitucion.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(9, 48)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(221, 23)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "CÓDIGO AMIE"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TxtCodigoAMIE
        '
        Me.TxtCodigoAMIE.Location = New System.Drawing.Point(12, 74)
        Me.TxtCodigoAMIE.Name = "TxtCodigoAMIE"
        Me.TxtCodigoAMIE.Size = New System.Drawing.Size(218, 20)
        Me.TxtCodigoAMIE.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(295, 48)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(221, 23)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "JORNADA"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CboJornada
        '
        Me.CboJornada.FormattingEnabled = True
        Me.CboJornada.Location = New System.Drawing.Point(298, 73)
        Me.CboJornada.Name = "CboJornada"
        Me.CboJornada.Size = New System.Drawing.Size(218, 21)
        Me.CboJornada.TabIndex = 5
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(12, 97)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(504, 23)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "PÁGINA WEB DE LA INSTITUCIÓN"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TxtPaginaWeb
        '
        Me.TxtPaginaWeb.Location = New System.Drawing.Point(12, 123)
        Me.TxtPaginaWeb.Name = "TxtPaginaWeb"
        Me.TxtPaginaWeb.Size = New System.Drawing.Size(504, 20)
        Me.TxtPaginaWeb.TabIndex = 7
        '
        'TxtTelefonos
        '
        Me.TxtTelefonos.Location = New System.Drawing.Point(12, 172)
        Me.TxtTelefonos.Name = "TxtTelefonos"
        Me.TxtTelefonos.Size = New System.Drawing.Size(504, 20)
        Me.TxtTelefonos.TabIndex = 9
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(12, 146)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(504, 23)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "TELÉFONOS DE LA INSTITUCIÓN"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(525, -1)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(182, 23)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "LOGOTIPO"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(559, 24)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(125, 125)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 11
        Me.PictureBox1.TabStop = False
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(528, 151)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(179, 23)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "CICLO ESCOLAR"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TxtCicloEscolar
        '
        Me.TxtCicloEscolar.Location = New System.Drawing.Point(544, 172)
        Me.TxtCicloEscolar.Name = "TxtCicloEscolar"
        Me.TxtCicloEscolar.Size = New System.Drawing.Size(150, 20)
        Me.TxtCicloEscolar.TabIndex = 13
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(12, 195)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(504, 23)
        Me.Label8.TabIndex = 14
        Me.Label8.Text = "NOMBRE DE LA AUTORIDAD"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TxtNombreAutoridad
        '
        Me.TxtNombreAutoridad.Location = New System.Drawing.Point(12, 221)
        Me.TxtNombreAutoridad.Name = "TxtNombreAutoridad"
        Me.TxtNombreAutoridad.Size = New System.Drawing.Size(504, 20)
        Me.TxtNombreAutoridad.TabIndex = 15
        '
        'btnRegistrarUsuarios
        '
        Me.btnRegistrarUsuarios.BackColor = System.Drawing.Color.SteelBlue
        Me.btnRegistrarUsuarios.FlatAppearance.BorderSize = 0
        Me.btnRegistrarUsuarios.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnRegistrarUsuarios.Font = New System.Drawing.Font("Arial Black", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRegistrarUsuarios.ForeColor = System.Drawing.Color.White
        Me.btnRegistrarUsuarios.Location = New System.Drawing.Point(54, 247)
        Me.btnRegistrarUsuarios.Name = "btnRegistrarUsuarios"
        Me.btnRegistrarUsuarios.Size = New System.Drawing.Size(180, 48)
        Me.btnRegistrarUsuarios.TabIndex = 16
        Me.btnRegistrarUsuarios.Text = "REGISTRAR USUARIOS"
        Me.btnRegistrarUsuarios.UseVisualStyleBackColor = False
        '
        'btnActualizar
        '
        Me.btnActualizar.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.btnActualizar.FlatAppearance.BorderSize = 0
        Me.btnActualizar.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnActualizar.Font = New System.Drawing.Font("Arial Black", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnActualizar.ForeColor = System.Drawing.Color.White
        Me.btnActualizar.Location = New System.Drawing.Point(406, 247)
        Me.btnActualizar.Name = "btnActualizar"
        Me.btnActualizar.Size = New System.Drawing.Size(110, 48)
        Me.btnActualizar.TabIndex = 17
        Me.btnActualizar.Text = "ACTUALIZAR"
        Me.btnActualizar.UseVisualStyleBackColor = False
        '
        'btnSalir
        '
        Me.btnSalir.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnSalir.Image = CType(resources.GetObject("btnSalir.Image"), System.Drawing.Image)
        Me.btnSalir.Location = New System.Drawing.Point(646, 247)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(48, 48)
        Me.btnSalir.TabIndex = 21
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'ToolTip1
        '
        Me.ToolTip1.IsBalloon = True
        '
        'btnRegistrarAreas
        '
        Me.btnRegistrarAreas.BackColor = System.Drawing.Color.Gray
        Me.btnRegistrarAreas.FlatAppearance.BorderSize = 0
        Me.btnRegistrarAreas.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnRegistrarAreas.Font = New System.Drawing.Font("Arial Black", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRegistrarAreas.ForeColor = System.Drawing.Color.White
        Me.btnRegistrarAreas.Location = New System.Drawing.Point(236, 247)
        Me.btnRegistrarAreas.Name = "btnRegistrarAreas"
        Me.btnRegistrarAreas.Size = New System.Drawing.Size(168, 48)
        Me.btnRegistrarAreas.TabIndex = 22
        Me.btnRegistrarAreas.Text = "REGISTRAR AREAS"
        Me.btnRegistrarAreas.UseVisualStyleBackColor = False
        '
        'frmPersonalizar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(706, 305)
        Me.Controls.Add(Me.btnRegistrarAreas)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnActualizar)
        Me.Controls.Add(Me.btnRegistrarUsuarios)
        Me.Controls.Add(Me.TxtNombreAutoridad)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.TxtCicloEscolar)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.TxtTelefonos)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.TxtPaginaWeb)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.CboJornada)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TxtCodigoAMIE)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TxtNombreInstitucion)
        Me.Controls.Add(Me.Label1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPersonalizar"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "DATOS DE LA INSTITUCION"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TxtNombreInstitucion As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TxtCodigoAMIE As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents CboJornada As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TxtPaginaWeb As System.Windows.Forms.TextBox
    Friend WithEvents TxtTelefonos As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TxtCicloEscolar As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TxtNombreAutoridad As System.Windows.Forms.TextBox
    Friend WithEvents btnRegistrarUsuarios As System.Windows.Forms.Button
    Friend WithEvents btnActualizar As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents btnRegistrarAreas As System.Windows.Forms.Button
End Class
