﻿Public Class frmMain

    Private Sub frmMain_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        End
    End Sub

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = VGTituloSoftware.ToString
        '''' Averiguar como cargar una imagen dinamicamente en un picturebox con visual basic .net y access
        '--------------------------------------------------------------------------------------------------
        '''' 
        ' Recuperar datos de la institución
        Dim consulta As String = "SELECT * FROM institucion"
        Dim adaptador As New OleDb.OleDbDataAdapter(consulta, cn)
        Dim registro As New DataSet
        adaptador.Fill(registro, "institucion")
        ' Desplegar los datos de la institución
        lblNombre.Text = registro.Tables("institucion").Rows(0).Item("nombre")
        lblCodAmie.Text = registro.Tables("institucion").Rows(0).Item("codigo_amie")
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        End
    End Sub

    Private Sub btnPersonalizar_Click(sender As Object, e As EventArgs) Handles btnPersonalizar.Click
        frmPersonalizar.ShowDialog()
    End Sub

    Private Sub BtnAsignaturas_Click(sender As Object, e As EventArgs) Handles btnAsignaturas.Click
        frmAsignaturas.ShowDialog()
    End Sub
End Class