﻿Module Miscelaneos
    Public Function autoCompletarTextBox(ByVal campoTexto As TextBox, ByVal nomCampo As String, ByVal nomTabla As String) As AutoCompleteStringCollection
        Dim de As New OleDb.OleDbDataAdapter("SELECT [" & nomCampo & "] AS campo FROM " & nomTabla, cn)
        Dim dt As New DataTable
        de.Fill(dt)

        Dim coleccion As New AutoCompleteStringCollection
        For Each row As DataRow In dt.Rows
            coleccion.Add(Convert.ToString(row("campo")))
        Next
        Return coleccion
    End Function
    Public Function FPBuscarCampo(tabla As String, campo As String, descripcion As String)
        VGStrSQL$ = "SELECT * FROM " + Trim(tabla) + " WHERE " + Trim(campo) + " = '" + descripcion + "'"
        Dim adaptador As New OleDb.OleDbDataAdapter(VGStrSQL, cn)
        Dim registro As New DataSet
        adaptador.Fill(registro, "tabla")
        Dim NumRegistros As Byte = registro.Tables("tabla").Rows.Count
        FPBuscarCampo = IIf(NumRegistros = 0, False, True)
    End Function
End Module
