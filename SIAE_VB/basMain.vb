﻿Module basMain
    Public cs As String
    Public cn As New OleDb.OleDbConnection()

    Public VGPasswd As String
    Public VGUsuario As String
    Public VGCodUsuario As Long

    ' Variables globales
    Public VGNivelSeg As Integer
    Public VGStrSQL, Sql As String
    Public VGTituloSoftware As String = "Sistema Integrado de Administración Estudiantil"
    Public VGCodNivSeg()

    Public VGCodUsuarioSel
    Public VGCodAreaSel
    Public Sub Conectar()
        Dim sPath_Access As String
        ' -- Ruta del Archivo ACCDB
        sPath_Access = Application.StartupPath & "\rubrica.accdb"
        ' MessageBox.Show(sPath)
        cs = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & _
             sPath_Access & ";Persist Security Info=False;"
        Try
            cn.ConnectionString = cs
            cn.Open()

            ' Si no salta algún error la conexión fue exitosa
        Catch ex As Exception
            ' Existe un error
            MessageBox.Show(ex.Message)
        End Try
    End Sub

End Module
